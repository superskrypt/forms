import { getEl, deepObjectExtend } from '@superskrypt/sutils';

import Button from './Button';

const defaults = {
	labels: {
		sending: 'Przesył danych',
		success: 'Zapisano'
	}
};

export default class FormButton extends Button {

	/**
	 *
	 * @param {*} el elementSelector or HTMLElement
	 * @param {*} options
	 */
	constructor( el, options ) {

		super( el, deepObjectExtend( defaults, options ) );

		this.input = getEl( 'input', this.el );

		if ( this.options || this.el ) {

			this.initFormButton();

		} else {

			console.log( 'FORMBUTTON ELEMENT DOESNT EXIST' );

		}

	}

	initFormButton() {

		if ( this.options.form ) {

			this.form = this.options.form;

		} else {

			// sometimes our passed element might be a wrapper
			// for the <input type="submit"
			if ( this.el.nodeName !== 'INPUT' ) {

				this.form = this.input.form.classRef;

			}

		}

		this.placeholder = getEl( '.form-button__placeholder', this.el );
		this.currentUIClass = '';

		this.formSubmittingHandler = this.formSubmittingHandler.bind( this );
		this.formValidationErrorHandler = this.formValidationErrorHandler.bind( this );
		this.formSendingHandler = this.formSendingHandler.bind( this );
		this.formSentHandler = this.formSentHandler.bind( this );

		this.initFormButtonEvents();

	}

	initFormButtonEvents() {

		this.form.UI.on( 'errorsFixed', this.setDefaultState.bind(this) );
		this.form.on( 'editable:change', this.setDefaultState.bind(this) );

	}

	setDefaultState() {

		this.setState( {
			name: 'default',
			label: this.options.labels.default
		} );

	}

	clickHandler( e ) {

		e.preventDefault();
		e.stopPropagation();

		this.form.on( 'submitting', this.formSubmittingHandler );

		if ( this.options.action ) {

			this.form.trigger( this.options.action );

		}

	}

	formSubmittingHandler( e ) {

		this.form.off( 'submitting', this.formSubmittingHandler );

		this.form.on( 'validationError', this.formValidationErrorHandler );
		this.form.on( 'sending', this.formSendingHandler );

	}

	formSendingHandler( e ) {

		// this.form.off( 'validationError', this.formValidationErrorHandler );
		// this.form.off( 'sending', this.formSendingHandler );

		this.setState( {
			name: 'sending',
			label: this.options.labels.sending
		} );

		this.form.on( 'sent', this.formSentHandler );

	}

	formSentHandler( e ) {

		this.setState( {
			name: 'success',
			label: this.options.labels.success
		} );

		this.form.off( 'sent', this.formSentHandler );

	}

	formValidationErrorHandler( e ) {

		this.form.off( 'validationError', this.formValidationErrorHandler );
		this.form.off( 'sending', this.formSendingHandler );

		this.setState( {
			name: 'error',
			label: 'Popraw dane'
		} );

	}

	/**
	 * [setState description]
	 * @param {string} options.name  name, also used as a class on the button
	 * @param {string} options.label displayed as a text value
	 */
	setState( { name, label } ) {

		this.setUIClass( name );
		this.input.value = label;

		if(this.placeholder) {
			this.placeholder.innerHTML = label;
		}

	}

	setUIClass( name ) {

		if ( this.currentUIClass.length > 0 ) {

			this.el.classList.remove( this.currentUIClass );

		}

		this.el.classList.add( name );
		this.currentUIClass = name;

	}

}
