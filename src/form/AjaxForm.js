import axios from 'axios';

import { extend, forEach, getEl } from '@superskrypt/sutils';
import Form from './Form';
import StopPromiseEvaluationError from './StopPromiseEvaluationError';

export default class AjaxForm extends Form {

    constructor( $el, endpoint, options ) {

        super( $el, options );
        this.endpoint = endpoint;

    }

    initEvents() {

        super.initEvents();

        // DOM EVENTS
        this.form.addEventListener( 'submit', this.submitHandler.bind( this ) );

        // CUSTOM EVENTS
        this.on( 'submit', this.submit );
        this.on( 'addNew', this.addNew );
        this.on( 'save', this.addNew );
        this.on( 'saveEditOffer', this.saveEditOffer );
        this.on( 'saveDraft', this.saveDraft );
        this.on( 'savePrivate', this.savePrivate );
        this.on( 'submitForReview', this.submitForReview );
        this.on( 'publish', this.publish );
        this.on( 'publishOffer', this.publishOffer );
        this.on( 'duplicateOffer', this.duplicateOffer );
        this.on( 'resumeOffer', this.resumeOffer );
        this.on( 'pauseOffer', this.pauseOffer );
        this.on( 'archiveOffer', this.archiveOffer );
        this.on( 'deleteOffer', this.deleteOffer );
    }

	submitHandler( e ) {

        e.preventDefault();

        this.trigger( 'submit' );

    }

    addNew( extraData ) {

        this.submit( {
            extraData: {
                ...extraData,
                status: 'publish'
            },
        } );

    }

    saveEditOffer( extraData ) {

        this.submit( {
            extraData: {
                ...extraData,
                status: 'publish'
            },
        } );

    }

    saveDraft() {

        this.submit();

    }

    submitForReview() {

        this.submit( {
            extraData: {
                status: 'pending'
            }
        },  );

    }

    savePrivate() {

        this.submit( {
            extraData: {
                status: 'private'
            }
        },  );

    }

    publish() {

        this.submit( {
            extraData: {
                status: 'publish'
            }
        },  );

    }

    publishOffer() {}
    duplicateOffer() {}
    resumeOffer() {}
    pauseOffer() {}
    archiveOffer() {}
    deleteOffer() {}

    submit( reqOptions = {}, endpoint = this.endpoint, method = 'POST' ) {


        const postId = this.getPostId();

        if ( postId ) {

            endpoint = `${endpoint}/${postId}`;

        }

        this.trigger( 'submitting' );

        this.processTheForm( reqOptions )
        .catch( this.formProcessingFail.bind( this ) )
        .then( ( data ) => {

            return this.send( data, endpoint, method );

        } )
        .then( this.formSubmitSuccess.bind( this ) )
        .catch (this.submitFailureHandler.bind(this))
        .catch (x=>{console.log(x)})
        ;

    }

    submitFailureHandler( errors ) {

        console.log(errors)

        if (errors instanceof StopPromiseEvaluationError) throw errors;

        forEach(errors.response.data, err => {
            var input = getEl('input[name="' + err.field + '"]');
            const validation = {
                result: false,
                messages: [err.msg],
            };
            input.ui.trigger('validate', {input: input.ui, validation});
        });
        this.trigger( 'validationError' );
    }

    processTheForm( reqOptions ) {

        this.trigger( 'processing' );

        // we can set arbitrary inputs that require validation
        // that way we can override default form Validation
        const inputsToValidate = reqOptions.inputsToValidate
            ? reqOptions.inputsToValidate
            : this.inputGroups;

        if ( this.sending === false && Form.validateInputGroups( inputsToValidate ) ) {

            const formData = Form.getData( this.inputGroups );

            // if there are extra data passed on submit options
            const data = reqOptions ? extend( formData, reqOptions.extraData ) : formData ;

            return Promise.resolve( this.processDataBeforeSending(data) );

        }

        return Promise.reject( new Error( 'The form did not validate or is submitting' ) );

    }

    processDataBeforeSending(data) {
        return data;
    }

    send( data, endpoint = this.endpoint, method = 'POST' ) {

        this.trigger( 'sending' );

        return new Promise( ( resolve, reject ) => {

            // uncomment for testing purposes
            // setTimeout(() => {
            // 	this.trigger( 'sent' );
            // 	resolve( true );
            // },1000)

            axios[method.toLocaleLowerCase()]( endpoint, data, {
                headers: {
                    'X-WP-Nonce': PwkApi.nonce
                },
            } ).then( response => {

                this.trigger( 'sent' );

                resolve( response );

            } ).catch( error => {

                reject( error );

            } );

        } );

    }

    formProcessingFail( error ) {
        this.trigger( 'validationError' );
        throw new StopPromiseEvaluationError();
    }

    resetInputs() {

        console.log( 'clearInputs called' );

        forEach( Object.entries( this.inputGroups ), ( [groupName, inputGroup] ) => {

            inputGroup.clear();

        } );

    }

    formSubmitSuccess( response ) {

        this.trigger('submitSuccess', response);
        this.sending = false;

    }

    formSubmitFail( error ) {

        this.sending = false;

        console.log( error );

    }

    getPostId() {

        if ( this.postId ) {

            return this.postId;

        } else if ( this.form.dataset.postId ) {

            return this.form.dataset.postId;

        }

        return undefined;

    }

}
