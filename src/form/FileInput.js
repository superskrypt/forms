import { loadFile } from '@superskrypt/sutils';

import Input from './Input';
import FileInputUI from './FileInputUI';

export default class FileInput extends Input {

	constructor( inputEl ) {

		super( inputEl );

		this.initFileInputEvents();

		this.UI = new FileInputUI( this );

	}

	initFileInputEvents() {

		this.el.addEventListener( 'click', this.clickHandler.bind( this ) );

	}

	clickHandler( e ) {

		this.trigger( 'file:click', { input: this, event: e } );

	}

	changeHandler( e ) {

		loadFile( this.el.files[0] )
			.then( file => {

				this.trigger( 'file:change', { file, input: this } );

			} )
			.catch( error => {

				throw new Error( error );

			} );

	}

	// focus and blur are overwritten so those event don't propagate up
	// and don't trigger generic Input UI responses
	blurHandler() {}

	clear () {
		super.clear();
		this.UI.removeFile();
	}
}
