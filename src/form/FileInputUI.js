import { getEl, insertAfter } from '@superskrypt/sutils';

export default class FileInputUI {

	constructor( fileInput ) {

		this.el = fileInput.el;
		this.ref = fileInput;

		this.init();

	}

	init() {

		this.container = this.el.parentNode;
		this.previewImg = getEl( '.file-upload__preview', this.container );
		this.removeButton = getEl( '.clear-attachment', this.container );
		this.nameContainer = getEl( '.file-upload__name', this.container );
		this.image = getEl( '.file-upload__preview', this.container );

		this.removeFile = this.removeFile.bind( this );
		this.displayPreview = this.displayPreview.bind( this );
		
		this.initEvents();
		this.listenToFileInputEvents();

	}

	initEvents() {

		this.removeButton.addEventListener( 'click', this.removeFile );

	}

	listenToFileInputEvents() {

		this.ref.on( 'file:change', this.changeHandler.bind( this ) );

	}

	changeHandler( { file } ) {

		this.displayPreview( file );

	}

	displayPreview( image ) {

		this.container.classList.add( 'success' );
		this.image = insertAfter( image, this.el );
		this.image.classList.add( 'file-upload__preview' );
		// this.nameContainer.innerHTML = this.el.files[0].name;

		this.removeButton.addEventListener( 'click', this.removeFile );

	}

	removeFile( ) {

		this.container.classList.remove( 'success' );
		if(this.image){
			this.image.remove();
		}
		this.removeButton.removeEventListener( 'click', this.removeFile );
		this.nameContainer.innerHTML = "";

		// this only makes sense for when we have 1 input per form
		this.el.form.reset();
		this.ref.trigger( 'file:remove');

	}

}
