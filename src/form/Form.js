import { getEl, getEls, forEach, extend, filter } from '@superskrypt/sutils';
import {Emitter} from '@superskrypt/sutils';

import InputFactory from './InputFactory';
import InputGroup from './InputGroup';
import FormUI from './FormUI';

const defaults = {};

export default class Form extends Emitter {

	/**
	 * Initialises the class
	 * @param  {string or DOMElement} $el   a selector or passed DOMElement of the
	 *                                      <form> we want to handle
	 * @return this
	 */
	constructor( $el, options ) {

		super();

		const form = getEl( $el );

		if ( form && form.nodeName === 'FORM' ) {

			this.init( form, options );

		} else {

			console.log( 'FORM ELEMENT DOESNT EXIST' );

		}

	}

	init( formEl, options ) {

		this.form = formEl;
		this.form.classRef = this; // this is so we can access the Form instance from the DOM ELEMENT
		this.inputs = this.form.elements;
		this.editForm = getEl( '.edit-form', this.el );
		this.editable = true;

		this.options = extend( defaults, options );

		this.sending = false;

		// prepare the logic side of things
		this.inputGroups = Form.prepareInputGroups( this.inputs );

		// we need to handle a case where we need to apply some logic
		// to a collection of InputGroups
		// A Good example is a collection of consent checkboxes
		// it sort of goes against the principle of no UI code in the Form
		// implementation, however at present I can't see a way to
		// have it done differently
		// this.inputGroupsCollections =

		// this.validity = this.spawnValidState( this.inputGroups )

		this.UI = new FormUI( this );

		this.initEvents();

	}

	spawnValidState ( inputGroups ) {

		let state = {};

		Object.entries( inputGroups ).forEach( ( [ key,inputGroup ] ) => {

			state[key] = false;

		} );

		return state;

	}

	// mapToValidState( inputGroupName, validation ) {

	// 	this.validity[inputGroupName] = validation.result;

	// }

	initEvents() {

		// const entries = Object.entries( this.inputGroups );

		// entries.forEach( ( [key, inputGroup] ) => {

		// 	inputGroup.on( 'validate', ( { validation } ) => {

		// 		if ( validation.result === false ) {

		// 			inputGroup.on( 'inputValidate', ( { validation } ) => {

		// 				console.log(validation.result)

		// 			} );

		// 		} else {

		// 		}

		// 		this.mapToValidState( key, validation );

		// 	} );

		// } );

	}

    get isFormValid( ) {

        return Form.validateInputGroups( this.inputGroups );

    }

	static prepareInputGroups( inputs ) {

		const filtered = Form.filterInputs( inputs );

		const wrapped = filtered.map( input => {

			return InputFactory.create( input );

		} );

		const grouped = Form.groupInputs( wrapped );

		const inputGroups = {};

		Object.entries( grouped ).forEach( ( [key, groupedInputs] ) => {

			inputGroups[key] = new InputGroup( groupedInputs );

		} );

		return inputGroups;

	}

	static filterInputs( inputs ) {

		return filter( inputs, input => {

			return input.type !== 'submit';

		} );

	}

	static groupInputs( inputs ) {

		return inputs.reduce( ( total, input ) => {

			( total[input.el.name] || ( total[input.el.name] = [] ) )
				.push( input );

			return total;

		}, {} );

	}

    static validateInputGroups( inputGroups ) {

        let valid = true;
        let groups;

        if ( inputGroups instanceof Array ) {

        	groups = inputGroups;

        } else if ( inputGroups instanceof Object ) {

        	const entries = Object.entries( inputGroups );

        	groups = entries.map( ( [key, inputGroup] ) => inputGroup );

        }

        groups.forEach( inputGroup => {

            if ( inputGroup.isValid === false ) {

                valid = false;

            }

        } );

        return valid;

    }



	toggleEditable() {

		if ( this.editable === true ) {

			this.setEditableOff();

		} else {

			this.setEditableOn();

		}

	}

	setEditableOn() {

		this.editable = true;

		this.trigger( 'editable:change', this.editable );

	}

	setEditableOff() {

		this.editable = false;

		this.trigger('editable:change', this.editable);

	}

	static getData( inputGroups ) {

		const data = {};

		forEach( Object.keys( inputGroups ), key => {

			data[key] = inputGroups[key].value;

		} );

		return data;

	}

}
