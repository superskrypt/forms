import { forEach, getEl, getEls, getParent, getClosest } from '@superskrypt/sutils';
import {Emitter} from '@superskrypt/sutils';
import RowUI from './RowUI';

export default class FormUI extends Emitter {

	/**
	 * [constructor description]
	 * @param  {Class Form} form   an instance of Class Form
	 * @return {[type]}      [description]
	 */
	constructor( form ) {

		super();

		this.form = form;

		const rowMap = this.extractRows( form );
		this.rows = this.buildRows( rowMap );


		this.attachRowsEventsAfterFormSubmit = this.attachRowsEventsAfterFormSubmit.bind( this );

		// this.form.on( 'submitting', this.attachRowsEventsAfterFormSubmit );
		this.form.on( 'editable:change', this.editableChangeHandler.bind(this) );
	}

	/**
	 * Extracts UI Rows from Form Instance
	 * @param  {Form} form
	 * @return {Map}  row -> [...inputGroups]
	 */
	extractRows( form ) {

		const rows = new Map();

		forEach( Object.entries( form.inputGroups ), ( [key, inputGroup] ) => {

			const row = getClosest( inputGroup.inputs[0].el, '.row' );

			if ( row ) {

				if ( rows.has( row ) ) {

					rows.set( row, [...rows.get( row ), inputGroup] );

				} else {

					rows.set( row, [inputGroup] );

				}

			}

		} );

		return rows;

	}

	/**
	 * Creates a collection of RowUI
	 * @param  {WeakMap} rowMap   mapping of .offer__row => [...InputGroups in the .offer__row]
	 * @return {Array}
	 */
	buildRows( rowMap ) {

		const rows = [];

		rowMap.forEach( ( inputGroups, row ) => {

			rows.push( new RowUI( row, inputGroups ) );

		} );

		return rows;

	}

	showValidationError() {

	}

	attachRowsEventsAfterFormSubmit() {

		forEach( this.rows, rowUI => {

			// rowUI.on( 'hideError', () => {

			// 	if ( this.form.isFormValid ) {

			// 		this.form.off( 'submit', this.attachRowsEventsAfterFormSubmit );
			// 		this.trigger( 'errorsFixed' );

			// 	}

			// } );

		} );

	}

	editableChangeHandler( editable ) {

		if ( editable === true ) {

			this.enableFormEditing();

		} else {

			this.disableFormEditing();

		}

	}

	enableFormEditing() {

		this.form.form.classList.remove( 'disabled' );
		this.form.form.classList.add( 'editable' );
		// this.form.form.previousElementSibling.classList.add( 'editable' );

	}

	disableFormEditing() {

		this.form.form.classList.add( 'disabled' );
		this.form.form.classList.remove( 'editable' );
		// this.form.form.previousElementSibling.classList.remove( 'editable' );

	}

}
