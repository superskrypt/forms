import rules from '../validation/rules';
import ValidationConfig from '../validation/config';
import { forEach, render, getClosest } from '@superskrypt/sutils';
import {Emitter} from '@superskrypt/sutils';

export default class Input extends Emitter {

	constructor( inputEl ) {

		super();

		this.el = inputEl;
		this.el.ui = this;
		this.errored = false;
		this.filled = false;

		this.config();
		this.initInputEvents();

	}

	config() {

		this.validationConfig = new ValidationConfig( this.el );
		this.validates = this.validationConfig.length > 0;
		this.inputAfterErrorHandler = this.inputAfterErrorHandler.bind(this);
	}


	initInputEvents() {

		this.el.addEventListener( 'focus', this.focusHandler.bind( this ) );
		this.el.addEventListener( 'blur', this.blurHandler.bind( this ) );
		this.el.addEventListener( 'input', this.inputHandler.bind( this ) );
		this.el.addEventListener( 'change', this.changeHandler.bind( this ) );
		this.el.addEventListener( 'click', this.clickHandler.bind( this ) );
		this.el.addEventListener( 'animationstart', this.animationStartHandler.bind( this ) );

		this.on( 'validate', this.validateHandler.bind( this ) );

	}


	focusHandler( e ) {

		if ( e.sourceCapabilities !== null ) {

			this.trigger( 'focus', { input: this, event: e } );

		}

	}

	clickHandler( e ) {

		this.trigger( 'click', { input: this, event: e } );

	}

	blurHandler( e ) {

		this.trigger( 'blur', { input: this, event: e } );

	}

	// a default input event handler
	inputHandler( e ) {

		if ( !this.filled && this.el.value.length > 0 ) {

			this.setFilled();

		} else if ( this.filled && this.el.value.length === 0 ) {

			this.setEmpty();
		}

	}

	// an input error handler
	// attached to the el only after validation fails
	// as we want the user to see the input getting validated
	// as the user types
	inputAfterErrorHandler( e ) {

		this.trigger( 'change', { input: this, event: e } );
		this.validate();

	}

	changeHandler( e ) {

		this.trigger( 'change', { input: this, event: e } );

		const { result } = this.validate();

		if ( this.errored === true && result === true ) {

			this.el.removeEventListener( 'input', this.inputAfterErrorHandler );

		}

	}

	animationStartHandler( { animationName } ) {

		if ( animationName === 'onAutoFillStart' ) {

			this.trigger( 'autofill' );

		}

	}

	validateHandler( { validation } ) {

		const { result } = validation;

		// if current field did not validate
		// change the validation trigger Event to onchange
		if ( this.errored === false && result === false ) {

			this.errored = true;
			this.el.addEventListener( 'input', this.inputAfterErrorHandler );

		}

	}

	setFilled() {

		this.el.classList.add('filled');
		this.filled = true;

		this.trigger( 'filled', { input: this, filled: this.filled } );

	}

	setEmpty() {

		this.el.classList.remove('filled');

		this.filled = false;

		this.trigger( 'filled', { input: this, filled: this.filled } );

	}


	clear() {

		this.el.value = '';

	}

	// validates against validation Config
	validate() {

		let result = true;
		const messages = [];

		this.validationConfig.forEach( rule => {

			if ( !rule.validator( this.el ) ) {

				result = false;
				messages.push( rule.message( this.el ) );

			}

		} );

		const validation = {
			result,
			messages
		};

		if ( this.validates ) {

			this.trigger( 'validate', { input: this, validation } );

		}

		return validation;

	}

	// maybe it should return bool instead of object
	get isValid() {

		return this.validate();

	}

	setRowFilled() {
		getClosest(this.el, '.row').classList.add('filled');
	}

	setRowNotFilled() {
		getClosest(this.el, '.row').classList.remove('filled');		
	}
}
