import TextInput from './TextInput';
import TextareaInput from './TextareaInput';
import CheckboxInput from './CheckboxInput';
import RadioInput from './RadioInput';
import NumberInput from './NumberInput';
import FileInput from './FileInput';
import PasswordInput from './Password';

const inputs = {};

class InputFactory {

	static register( inputType, inputClass ) {

		inputs[inputType] = inputClass;

	}

	static create( inputEl ) {

		const Creatable = inputs[inputEl.type];

		return new Creatable( inputEl );

	}

}

InputFactory.register( 'text', TextInput );
InputFactory.register( 'email', TextInput );
InputFactory.register( 'tel', TextInput );
InputFactory.register( 'url', TextInput );
InputFactory.register( 'hidden', TextInput );
InputFactory.register( 'password', PasswordInput );
InputFactory.register( 'textarea', TextareaInput );
InputFactory.register( 'checkbox', CheckboxInput );
InputFactory.register( 'radio', CheckboxInput );
InputFactory.register( 'number', NumberInput );
InputFactory.register( 'file', FileInput );

export default InputFactory;
