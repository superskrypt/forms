import {Emitter, getParent, getClosest} from '@superskrypt/sutils';
import ValidationConfig from '../validation/config';

export default class InputGroup extends Emitter {

	constructor( inputs ) {

		super();

		this.inputs = [];
		this.type = false;

		this.init( inputs );

	}

	init ( inputs ) {

		inputs.forEach( input => {

			this.push( input );

		} );

		this.wrapper = getClosest( this.inputs[0].el, '.input-group' );

		if ( this.wrapper ) {

			this.wrapper.classRef = this;

		}

		this.initGroupValidators();

	}

	push( input ) {

		this.inputs.push( input );

		if ( this.type === false ) {

			this.type = input.el.type;

		} else if( this.type !== input.el.type ) {

			throw new TypeError( 'input in inputGroup should be of same type' );

		}
		// this is so we can propagate the state of the input
		// higher up the Components Hierarchy
		this.attachInputListeners( input );

	}

	attachInputListeners( input ) {

		input.on( 'focus', this.inputFocusHandler.bind( this ) );
		input.on( 'blur', this.inputBlurHandler.bind( this ) );
		input.on( 'change', this.inputChangeHandler.bind( this ) );
		input.on( 'validate', this.inputValidateHandler.bind( this ) );
		input.on( 'click', this.clickHandler.bind( this ) );
		input.on( 'autofill', this.inputChangeHandler.bind( this ) );
		input.on( 'filled', this.inputFilledHandler.bind( this ) );

		// Events related to specific input type
		input.on( 'checkbox:click', this.clickHandler.bind( this ) );
		input.on( 'file:click', this.fileClickHandler.bind( this ) );
		input.on( 'file:change',  this.fileChangeHandler.bind( this ) );
		input.on( 'file:remove',  this.fileRemoveHandler.bind( this ) );
		input.on( 'password:change',  this.passwordChangeHandler.bind( this ) );

	}

	inputClickHandler( eventData ) {

		this.trigger( 'focus', eventData );

	}

	inputFocusHandler( eventData ) {

		this.trigger( 'focus', eventData );

	}

	inputBlurHandler( eventData ) {

		this.trigger( 'blur', eventData );

	}

	inputChangeHandler( eventData ) {


		this.trigger( 'change', eventData );

	}

	inputValidateHandler( eventData ) {

		this.trigger( 'inputValidate', { inputGroup: this, ...eventData } );

	}

	inputFilledHandler( eventData ) {


		this.trigger( 'inputFilled', { inputGroup: this, ...eventData } );

	}

	clickHandler( eventData ) {

		// this.trigger( 'checkbox:click', eventData );
		this.trigger( 'click', eventData );

	}

	fileClickHandler( eventData ) {

		this.trigger( 'file:click', eventData );

	}

	fileRemoveHandler( eventData ) {

		this.trigger( 'file:remove', eventData );

	}

	fileChangeHandler( eventData ) {

		this.trigger( 'file:change', eventData );

	}

	passwordChangeHandler( eventData ) {

		this.trigger( 'password:change', eventData );

	}


	inputToProxy( input ) {

		return this.inputs.filter( inputProxy => {

			return inputProxy.id === input.el.id;

		} )[0];
	}

	initGroupValidators() {

		if ( this.wrapper ) {

			this.validationConfig = new ValidationConfig( this.wrapper );
			this.validates = this.validationConfig.length > 0;

		}

	}

	validateInputs() {

		const inputsLen = this.inputs.length;
		let valid = true;
		let i = 0;
		let lastValidation = null;

		while ( valid && i < inputsLen ) {

			lastValidation = this.inputs[i].validate();

			const { result } = lastValidation;

			if ( result === false ) {

				valid = false;

			}

			i += 1;

		}

		return lastValidation;

	}

	validate() {

		// now let's handle InputGroup validation
		// but only if the Inputs within the group
		// did validate
		const messages = [];
		let valid = true;

		this.validationConfig.forEach( rule => {

			if ( !rule.validator( this.wrapper ) ) {

				valid = false;
				messages.push( rule.message( this.wrapper ) );

			}

		} );

		const lastValidation = {
			result: valid,
			messages
		};

		return lastValidation;

	}

	/**
	 * Checks for validity of every element in the Input Group
	 * @return {Boolean} [description]
	 */
	get isValid() {

		let lastValidation = { result: true };

		const inputsValidation = this.validateInputs();

		if ( inputsValidation.result === true && this.validates ) {

			const groupValidation = this.validate();

			if ( groupValidation.result === false ) {

				lastValidation = groupValidation;

			}

		} else {

			lastValidation = inputsValidation;

		}

		this.trigger( 'inputValidate', { validation: lastValidation } );

		return lastValidation.result;

	}

	get isValid2(){

		consthis.validateInputs()

	}

	/**
	 * A method return a shared <input>'s name attribute value
	 * @return {string} name of the Input Group
	 */
	get name() {

		return this.inputs.reduce( ( acc, input ) => {

			if ( acc.length > 0 && acc !== input.name ) {

				console.log( 'inconsistent naming for the InputGroup');

			}

			return input.name;

		}, '' );

	}

	get value() {

		let initAcc;
		let valueGetter;

		if ( this.type === 'checkbox'  ) {

			if ( this.inputs.length === 1 ) {

				initAcc = false;
				valueGetter = ( acc, input ) => {

					if ( input.el.checked === true ) acc = true;

					return acc;

				};

			} else {

				initAcc = [];
				valueGetter = ( acc, input ) => {

					if( input.el.checked === true ) {

						if ( input.el.value !== 'on' ) {

							acc.push( input.el.value );

						} else {

							acc.push( input.el.id );

						}


					}

					return acc;

				};

			}

		} else if ( this.type === 'radio' ) {

			initAcc = false;
			valueGetter = ( acc, input ) => {

				return input.el.checked === true ? input.el.value : acc;

			};

		} else {

			initAcc = '';
			valueGetter = ( acc, input ) => {

				return acc += input.el.value;

			};

		}

		const val = this.inputs.reduce( valueGetter, initAcc );

		return val;

	}

	clear() {

		this.inputs.forEach( input => {

			input.clear();

		} );

	}

}
