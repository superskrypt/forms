import { getParent, forEach } from '@superskrypt/sutils';
export default class InputGroupsCollection {

	constructor( inputGroups, parentSelector ) {

		this.collection = InputGroupsCollection.groupByCommonParent( inputGroups, parentSelector );

	}

	static groupByCommonParent( inputGroups, parentSelector ) {

		const collection = new Map();

		forEach( Object.entries( inputGroups ), ( [key, inputGroup] ) => {

			const row = getParent( parentSelector, inputGroup.inputs[0] );

			if ( row ) {

				if ( collection.has( row ) ) {

					collection.set( row, [...collection.get( row ), inputGroup] );

				} else {

					collection.set( row, [inputGroup] );

				}

			}

		} );

		return collection;

	}

}
