import Input from './Input';
import { getEl, getEls, getClosest, forEach } from '@superskrypt/sutils';
import ValidationConfig from '../validation/config';


export default class PasswordInput extends Input {

	constructor( inputEl ) {

		super( inputEl );

		this.filled = false;

	}

	clear() {

		this.el.value = '';

	}

	focusHandler( e ) {

		if ( this.el.classList.contains('filled') && this.el.dataset.hasOwnProperty('same') ) {
			this.el.addEventListener( 'change', this.passwordChanged.bind(this));
		}

		if ( e.sourceCapabilities !== null ) {

			this.trigger( 'focus', { input: this, event: e } );

		}

	}

	passwordChanged( e ) {
		this.el.classList.add('changed');
		let thisRow = getClosest(this.el, '.row');
		let form = getClosest(this.el, 'form');
		let hiddenInputs = getEls( '.hidden-until-previous-changed', form );
		if ( hiddenInputs ) {
			forEach( hiddenInputs, hiddenInput => {
				hiddenInput.classList.add('showed-input');
			} );
		}
		thisRow.classList.add('changed');
	}

}
