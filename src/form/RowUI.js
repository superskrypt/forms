import {Emitter} from '@superskrypt/sutils';
import { forEach, getEl, getEls } from '@superskrypt/sutils';

/**
 * This class will handle the visible side-effects
 * of processing a single Row of Form data
 *
 * In this case a Row is a strict representation of a UI
 * as well as UI Handler
 * It should be completely free of Form logic
 */

export default class RowUI extends Emitter {

	/**
	 * [constructor description]
	 * @param  {DOMElement} row
	 * @return {RowUI}
	 */
	constructor( row, inputGroups ) {

		super();

		this.el = row;
		this.inputGroups = inputGroups;

		this.label = getEl( '.row__label', this.el );
		this.hint = getEl( '.form__hint', this.el );
		this.hintSecond = getEl( '.form__hint-second', this.el );

		this.active = false;
		this.error = false;
		this.changed = false;

		this.config();

		this.listenToInputGroupsEvents();

		if ( this.label ) {

			this.listenToLabelEvents();

		}

		this.initEvents();

	}

	config() {

		this.inputBlurHandler = this.inputBlurHandler.bind( this );
		this.beforeBlurHandler = this.beforeBlurHandler.bind( this );
		this.afterBlurHandler = this.afterBlurHandler.bind( this );

	}

	initEvents() {

		this.el.addEventListener( 'click', this.rowClickHandler.bind( this ) );

	}


	listenToInputGroupsEvents() {

		forEach( this.inputGroups, inputGroup => {

			inputGroup.on( 'focus', this.inputFocusHandler.bind( this ) );
			inputGroup.on( 'blur', this.inputBlurHandler.bind( this ) );
			inputGroup.on( 'change', this.inputChangeHandler.bind( this ) );
			inputGroup.on( 'inputFilled', this.inputFilledHandler.bind( this ) );

			// handle inputGroup Validation
			inputGroup.on( 'validate', this.inputGroupValidateHandler.bind( this ) );

			// handle input Validation
			inputGroup.on( 'inputValidate', this.inputValidateHandler.bind( this ) );

			// Events related to specific input type
			inputGroup.on( 'click', this.clickHandler.bind( this ) );

			// the file input has same behaviour as checkbox
			inputGroup.on( 'file:click', this.fileClickHandler.bind( this ) );
			inputGroup.on( 'file:change', this.fileChangeHandler.bind( this ) );


		} );

	}

	listenToLabelEvents() {

		if ( this.label ) {

			this.label.addEventListener( 'click', this.labelClickHandler.bind( this ) );

		}

	}

	focusOnFirstInput( e ) {

		// console.log(e.target)

		this.inputGroups[0].inputs[0].el.focus();

	}

	rowClickHandler( e ) {

		if ( this.active === false ) {

			if ( e.target.nodeName === 'DIV' ) {

				this.focusOnFirstInput( e );

			}

			this.inputFocusHandler();

		}

	}

	inputFocusHandler( eventData ) {

		if ( !this.changed && this.hint ) {

			this.parkTheHint();

		}

		this.setActive();

		// let's add an event listener everywhere
		// so that's when we click out of the
		// checkbox input group the group goes inactive;
		document.addEventListener( 'mousedown', this.beforeBlurHandler );

	}

	inputBlurHandler( eventData ) {

		if ( eventData && this.el.contains( eventData.event.currentTarget ) ) {

			if ( eventData.event.relatedTarget === null ) {

				eventData.event.preventDefault();
				return;

			}

		}

		// we need to check whether the blur event originates
		// in the click Event
		// if it does let's wait for clickOut
		if ( !this.isWaitingForMouseup ) {

			document.removeEventListener( 'mousedown', this.inputBlurHandler );

			if ( !this.changed && this.hint ) {

				this.unparkTheHint();

			}

			this.setInactive();

		}

	}

	beforeBlurHandler( e ) {

		// check if the event target is in currently active row
		if ( !this.el.contains( e.target ) ) {

			this.isWaitingForMouseup = true;

			document.removeEventListener( 'mousedown', this.beforeBlurHandler );
			document.addEventListener( 'mouseup', this.afterBlurHandler );

		}

	}

	afterBlurHandler( e ) {

		if ( !this.el.contains( e.target ) ) {

			this.isWaitingForMouseup = false;

			this.inputBlurHandler();

			document.removeEventListener( 'mouseup', this.afterBlurHandler );

		}

	}

	inputChangeHandler( eventData ) {

		if ( this.changed === false && this.hint ) {

			this.hideError();
			this.hideHint();
			this.changed = true;

		}

	}

	fileChangeHandler( eventData ) {

		this.inputChangeHandler( eventData );

	}

	inputGroupValidateHandler( { validation } ) {

		if ( this.el.classList.contains( 'row--duration' ) ) {

			// console.log( this );

		}

		const { result, messages } = validation;

		if ( result === false ) {

			this.showError( messages );

		} else {

			this.hideError();

		}

	}

	inputValidateHandler( { event, input, validation } ) {

		const { result, messages } = validation;

		if ( result === false ) {

			this.showError( messages );

		} else {

			this.hideError();

		}

	}

	inputFilledHandler( eventData ) {

		const { filled } = eventData;

		if ( filled === true ) {

			this.setFilled();

		} else {

			this.removeFilled();

		}

	}

	clickHandler( eventData ) {

		this.inputFocusHandler( eventData );

		const clickedEl = eventData.input.el;

		if( clickedEl.parentNode.hasAttribute( 'toggleinput' ) ) {

			const toggleAttribute = clickedEl.parentNode.getAttribute( 'toggleinput' );
			const inputEl = getEl( 'input[type=text]', clickedEl.parentNode );
			const inputWrapper = inputEl.parentNode;

			inputEl.value = clickedEl.value != toggleAttribute ? '' : inputEl.value;

		}

	}

	fileClickHandler( eventData ) {

		this.inputFocusHandler( eventData );

	}

	labelClickHandler( e ) {

		return;

		const relatedInputs = getEls( `[name=${e.currentTarget.htmlFor}]` );

		if ( this.inputGroups.length > 1 || relatedInputs.length > 1 ) {

			// this.checkboxClickHandler();

		}

	}

	setActive() {

		if ( this.active === false ) {

			this.el.classList.add( 'active' );
			this.active = true;

		}

	}

	setInactive() {

		if ( this.active ) {

			this.el.classList.remove( 'active' );
			this.active = false;

		}

	}

	setFilled() {

		this.el.classList.add( 'filled' );

	}

	removeFilled() {

		this.el.classList.remove( 'filled' );

	}

	parkTheHint() {

		this.hint.classList.add( 'parked' );

	}

	unparkTheHint() {

		this.hint.classList.remove( 'parked' );

	}

	hideHint() {

		this.hint.innerText = '';

	}

	showError( messages ) {

		if ( !this.error ) {

			this.error = true;
			this.el.classList.add( 'error' );
			this.parkTheHint();

			if( this.hintSecond ) {
				this.hintSecond.innerHTML = '';
			}

		}

		this.hint.innerHTML = '';
		// take the first message only
		this.hint.innerHTML += `<p>${messages[0]}</p>`;

	}

	hideError() {

		if ( this.error ) {

			this.error = false;
			this.el.classList.remove( 'error' );
			this.hint.innerHTML = '';

			this.trigger( 'hideError' );

		}

	}

	showValidation() {

		console.log( 'showValidationErrors callse' );

	}

}
