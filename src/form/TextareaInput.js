// resize based on https://gist.github.com/ugin/5779160

import browser from '@superskrypt/sutils';
import { forEach, getEl, getEls, getClosest } from '@superskrypt/sutils';
import TextInput from './TextInput';

export default class TextareaInput extends TextInput {

	constructor( inputEl ) {

		super( inputEl );

		this.resize = this.resize.bind( this );
		this.initTextareaEvents();

	}

	initTextareaEvents() {

		this.el.addEventListener( 'change', this.resize.bind( this ) );
		this.el.addEventListener( 'cut', this.delayedResize.bind( this ) );
		this.el.addEventListener( 'paste', this.delayedResize.bind( this ) );
		this.el.addEventListener( 'drop', this.delayedResize.bind( this ) );
		this.el.addEventListener( 'keydown', this.delayedResize.bind( this ) );

		const oragniastionPanel = getEl('.organisation-panel');
		const editOffer = getEl('.edit-offer');
		const emptyHint = getEl('.form__hint', getClosest(this.el, '.row__inputs')).innerHTML == "";
		
		if( (oragniastionPanel || editOffer) && emptyHint) {
			window.addEventListener( 'load', this.setHeight() );
		}

	}


	resize( event ) {

		this.el.style.height = 'auto';
		this.el.style.height = `${this.el.scrollHeight}px`;

	}
	setHeight() {
		this.el.style.height = 'auto';
		this.el.style.height = `${this.el.scrollHeight}px`;
	}

	delayedResize( event ) {

		window.setTimeout( this.resize.bind( this ), 0, event );

	}

}
