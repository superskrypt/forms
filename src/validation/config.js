import rules from './rules';

const mapping = {

	// input rules
	required: input => input.hasAttribute( 'required' ),
	url: input => input.getAttribute( 'type' ) === 'url',
	email: input => input.getAttribute( 'type' ) === 'email',
	telephone: input => input.getAttribute( 'type' ) === 'tel',
	password: input => input.getAttribute( 'type' ) === 'password',
	minlength: input => input.getAttribute( 'minlength' ) !== null,
	maxlength: input => input.getAttribute( 'maxlength' ) !== null,
	optionalequalorsmallerthan: input => input.hasAttribute( 'optionalequalorsmallerthan' ),
	optionalequalorbiggerthan: input => input.hasAttribute( 'optionalequalorbiggerthan' ),
	// input group Rules
	minCheckedCount: inputGroup => inputGroup.hasAttribute( 'ig-min-checked-count' ),
	inputRequiredIfChecked: inputGroup => inputGroup.hasAttribute( 'input-required-if-checked' ),
	dataSameValues: inputGroup => inputGroup.dataset.hasOwnProperty('samevalue'),

};

/**
 * Creates new validation config objects
 */
export default class Config {

	/**
	 * initialise config for given element
	 * @param  {HTMLElement} el should be an Input/Textarea/Select ..etc
	 * or a .input-group wrapping the input
	 * @return {Array}    array of rules applicable for given element
	 */
	constructor( el ) {

		const elRules = [];

		Object.entries( mapping ).forEach( ( [key, mappingTest] ) => {

			if ( mappingTest( el ) ) {

				elRules.push( rules[key] );

			}

		} );

		return elRules;

	}

}
