import { getEl, getEls, getClosest, forEach } from '@superskrypt/sutils';

const labelName = input => {

	return getEls( 'label', getClosest(input, '.row') )[0].innerText.toLowerCase();

};


const getFile = input => {

	// if there is custom file upload logic, for example, images are resized client-side
    // generated Blobs should be assigned to fileInput._file
    // and can be sent via ajax with FormData

    // if file was deleted, custom field can be set to an empty string

    // Bunny Validation detects if there is custom Blob assigned to file input
    // and uses this file for validation instead of original read-only input.files[]
    if (input._file !== undefined && input._file !== '') {
        if (input._file instanceof Blob === false) {
            console.error(`Custom file for input ${input.name} is not an instance of Blob`);
            return false;
        }
        return input._file;
    }

    return input.files[0] || false;

};

const rules =  {
	required: {
		validator: input => {

			if ( input.getAttribute( 'type' ) !== 'file' && input.value === ''
                || ( ( input.type === 'radio' || input.type === 'checkbox' ) && !input.checked )
                || input.getAttribute( 'type' ) === 'file' && getFile( fileInput ) === false ) {

				return false;
			}

			return true;

		},
		message: input => {

			if ( input.getAttribute( 'type' ) === 'checkbox' ) {

				return 'Zaznaczenie pola jest wymagane';

			} else {

				return `Uzupełnienie pola ${labelName( input )} jest wymagane`

			}

		}
	},

	minlength: {
		validator: input => input.value.length >= input.getAttribute( 'minlength' ),
		message: input => `Pole ${labelName( input )} musi posiadać przynajmniej ${input.getAttribute( 'minlength' )} znaki`
	},
	maxlength: {
		validator: input => input.value.length < input.getAttribute( 'maxlength' ),
		message: input => `Pole ${labelName( input )} może zawierać maksymalnie ${input.getAttribute( 'maxlength' )} znaków`
	},
	telephone: {
		validator: input => {

			const phoneRegex = /^[0-9\+]{9,15}$/;
			return phoneRegex.test( input.value );

		},
		message: input => `Pole ${labelName( input )} może zawierać co najmniej 9 cyfr`
	},
	name: {
		validator: input => {

			const { value } = input;

			let flag = false;
			const splited = value.split( ' ' );

			if ( splited.length === 2 && splited[0].length > 1 && splited[1].length > 1 ) {

				flag = true;

			}

			return flag;

		},
		message: 'Imię i nazwisko muszą zawierać co najmniej po 2 znaki.'
	},
	password: {
		validator: input => {

			if ( input.value.length > 0 && input.getAttribute( 'type' ) === 'password' ) {

				const passwordsRegex = /(?=.{1,})/;

				return passwordsRegex.test( input.value );

			}

			return true;

		},
		message: input => `Minimalna długość hasła to 1 znak.`
	},
	email: {
		validator: input => {

			if ( input.value.length > 0 && input.getAttribute( 'type' ) === 'email' ) {

				const emailRegex = /^[-a-z0-9~!$%^&*_=+}{\'?]+(\.[-a-z0-9~!$%^&*_=+}{\'?]+)*@([a-z0-9_][-a-z0-9_]*(\.[-a-z0-9_]+)*\.(aero|arpa|biz|com|coop|edu|gov|info|int|mil|museum|name|net|org|pro|travel|mobi|[a-z][a-z])|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(:[0-9]{1,5})?$/i;

				return emailRegex.test( input.value );

			}

			return true;

		},
		message: input => `Pole ${labelName( input )} nie jest poprawnym adresem e-mail`
	},
	url: {

		validator: input => {

		if ( input.getAttribute( 'type' ) === 'url' ) {

			const urlRegex = /^(?:(?:(?:https?|ftp):)?\/\/)?(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z0-9\u00a1-\uffff][a-z0-9\u00a1-\uffff_-]{0,62})?[a-z0-9\u00a1-\uffff]\.)+(?:[a-z\u00a1-\uffff]{2,}\.?))(?::\d{2,5})?(?:[/?#]\S*)?$/i;

			return urlRegex.test( input.value );

		}
		
		return  true;

		},

		message: input => `Niepoprawny adres www`
	},
	optionalequalorsmallerthan: {
		validator: input => {

			const compareTo = ( input.getAttribute('optionalequalorsmallerthan') );
			const compareToEl = getEl( `#${compareTo}` );

			if ( compareToEl.value && parseInt( input.value, 10 ) > parseInt( compareToEl.value, 10 ) ) {

				return false;

			}

			return true;


		},
		message: input => {

			const compareTo = ( input.getAttribute('optionalequalorsmallerthan') );
			const compareToEl = getEl( `#${compareTo}` );

			return `Pole ${labelName( input )} powinno być większe niż pole  ${labelName( compareToEl )}`
		}
	},
	optionalequalorbiggerthan: {
		validator: input => {

			const compareTo = ( input.getAttribute('optionalequalorbiggerthan') );
			const compareToEl = getEl( `#${compareTo}` );

			if ( compareToEl.value && parseInt( input.value, 10 ) < parseInt( compareToEl.value, 10 ) ) {

				return false;

			}

			return true;


		},
		message: input => {

			const compareTo = ( input.getAttribute('optionalequalorbiggerthan') );
			const compareToEl = getEl( `#${compareTo}` );

			return `Pole ${labelName( input )} powinno być mniejsze niż pole  ${labelName( compareToEl )}`
		}
	},

	// input Group rules
	dataSameValues: {

		validator: inputGroup => {

			let sameValuesInputs = getEls( 'input.showed-input', inputGroup );

			let inputsValues =[];

			forEach( sameValuesInputs, sameValueInput => {
				inputsValues.push( sameValueInput.value );
			} );

			let sameInputsUniqueValues = inputsValues.filter(function(item, pos, self) {
			    return self.indexOf(item) == pos;
			})

			if ( sameInputsUniqueValues.length > 1 ) {
				return false;
			}

			return true;

		},

		message: inputGroup => {

			return `Pola muszą być takie same`;

		}
	},

	minCheckedCount: {
		validator: inputGroup => {

			const minCount = inputGroup.getAttribute( 'ig-min-checked-count' );
			const checked = getEls( ':checked', inputGroup );

			return checked.length >= minCount;

		},
		message: inputGroup => {

			const minCount = inputGroup.getAttribute( 'ig-min-checked-count' );
			const labelName = inputGroup.classRef.inputs[0].el.name;
			console.log(minCount, labelName, getEl( `label[for="${labelName}"]` ));
			const label = getEl( `label[for="${labelName}"]` ).innerText.toLowerCase();

			return `Proszę zaznaczyć co najmniej ${minCount} pole dla ${label}`;

		}
	},

	inputRequiredIfChecked: {
		validator: inputGroup => {
			const checkedId = inputGroup.getAttribute( 'input-required-if-checked' );
			const inputCheckedId = getEl(' input[type=text] ', inputGroup);
			const checked = getEls(' :checked ', inputGroup);

			if( checked.length >= 1 ) {

				if( checked[0].getAttribute('id') == checkedId ) {

					return inputCheckedId.value.trim();

				}

			} else {

				return false;

			}


			return true;

		},
		message: inputGroup => {

			const checkedId = inputGroup.getAttribute( 'input-required-if-checked' );
			const inputCheckedId = getEl(' input[type=text] ', inputGroup);
			const checked = getEls(' :checked ', inputGroup);
			const labelName = getEls(' input[type=radio] ', inputGroup)[0].name;
			const label = getEl( `label[for="${labelName}"]` ).innerText.toLowerCase();

			if( checked.length >= 1 ) {

				if( checked[0].getAttribute('id') == checkedId && !inputCheckedId.value.trim() ) {

					return `Proszę uzupełnić pole dla zaznaczonej opcji inny`;

				}

			} else {

				return `Proszę zaznaczyć co najmniej 1 pole dla ${label}`;

			}

		}
	}

};

export default rules;
